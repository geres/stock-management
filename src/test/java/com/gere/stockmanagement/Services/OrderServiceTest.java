package com.gere.stockmanagement.Services;

import com.gere.stockmanagement.Domain.ProductNotFoundException;
import com.gere.stockmanagement.Domain.InvalidQuantityException;
import com.gere.stockmanagement.Domain.Order;
import com.gere.stockmanagement.Domain.Product;
import com.gere.stockmanagement.Domain.Stock;
import com.gere.stockmanagement.Repositories.OrderRepository;
import com.gere.stockmanagement.Repositories.ProductRepository;
import com.gere.stockmanagement.Repositories.StockRepository;
import com.gere.stockmanagement.StockManagementApplication;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = StockManagementApplication.class)
class OrderServiceTest {
    @Autowired private StockRepository stockRepository;
    private Product product;
    private Stock stock;
    private OrderService sut;
    @Autowired private ProductRepository productRepository;
    @Autowired private OrderRepository orderRepository;
    @Autowired private StockService stockService;

    @BeforeEach
    void setUp() {
        this.product = new Product("name", "description");
        this.stock = new Stock();
        this.sut = new OrderService(this.orderRepository, this.stockService);
        this.product.addStock(stock);
        this.productRepository.save(product);
        this.stockRepository.save(stock);
    }
    
    private Stock getStock(long stockId) {
        return this.stockRepository.getOne(stockId);
    }

    @Test
    @Transactional
    void it_should_be_possible_to_execute_an_order() throws InvalidQuantityException {
        Order order = new Order(this.product.getId(), 10);

        this.sut.execute(order);
        Stock stock = this.getStock(order.getProductId());
        assertEquals(90, stock.getQuantity());
    }

    @Test
    @Transactional
    void it_should_not_accept_an_order_with_more_items_than_available() {
        Order order = new Order(this.product.getId(), 101);
        assertThrows(InvalidQuantityException.class, () -> this.sut.execute(order));
        Stock stock = this.getStock(order.getProductId());
        assertEquals(100, stock.getQuantity());
    }

    @Test
    @Transactional
    void it_should_not_be_possible_to_order_a_non_existent_product() {
        Order order = new Order(this.product.getId() + 1, 20);
        assertThrows(ProductNotFoundException.class, () -> this.sut.execute(order));
    }
}
