package com.gere.stockmanagement.Domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;

@Data
@Entity
public class Stock {

    @Id
    private long id;

    private int quantity;

    @OneToOne
    @MapsId
    private Product product;

    /**
     * Create a stock and default set it to 100
     */
    public Stock() {
        this(100);
    }

    /**
     * Create a stock with the given initial quantity
     *
     * @param quantity create a stock with a given number of items
     */
    private Stock(int quantity) {
        this.quantity = quantity;
    }

    /**
     * Check if the new stock is valid, and add it.
     *
     * @param refillQuantity restocked refillQuantity     *
     */
    public void refill(int refillQuantity) throws InvalidQuantityException {
        if (refillQuantity >= 0) {
            this.quantity += refillQuantity;
        } else {
            throw new InvalidQuantityException(refillQuantity);
        }
    }

    /**
     * Check if there is enough stock, and order it.
     *
     * @param orderQuantity ordered quantity
     */
    public void remove(int orderQuantity) throws InvalidQuantityException {
        if (orderQuantity >= 0 && (this.quantity - orderQuantity) >= 0) {
            this.quantity -= orderQuantity;
        } else {
            throw new InvalidQuantityException(orderQuantity);
        }
    }
}
