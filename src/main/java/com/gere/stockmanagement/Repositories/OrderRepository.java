package com.gere.stockmanagement.Repositories;

import com.gere.stockmanagement.Domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
}
