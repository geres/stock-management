package com.gere.stockmanagement.Domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;

    /**
     * zero-parameter constructor needed by Spring wiring
     */
    protected Product() {
    }

    /**
     * @param name        the name of the product
     * @param description a brief description
     */
    public Product(String name, String description) {
        this.name = name;
        this.description = description;
    }

    /**
     * @param stock
     */
    public void addStock(Stock stock) {
        stock.setProduct(this);
    }
}
