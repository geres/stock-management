package com.gere.stockmanagement.Services;

import com.gere.stockmanagement.Domain.InvalidQuantityException;
import com.gere.stockmanagement.Domain.Product;
import com.gere.stockmanagement.Domain.Stock;
import com.gere.stockmanagement.Repositories.ProductRepository;
import com.gere.stockmanagement.Repositories.StockRepository;
import com.gere.stockmanagement.StockManagementApplication;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = StockManagementApplication.class)
public class StockServiceTest {
    @Autowired private StockRepository stockRepository;
    @Autowired private ProductRepository productRepository;
    private StockService sut;
    private Stock stock;

    @BeforeEach
    void SetUp() {
        Product product = new Product("name", "description");
        this.stock = new Stock();
        product.addStock(stock);
        this.productRepository.save(product);
        this.stockRepository.save(stock);
        this.sut = new StockService(this.stockRepository);
    }

    @Test
    @Transactional
    void it_should_refill_the_stock() throws InvalidQuantityException {
        Stock newStock = this.sut.refill(this.stock.getId(), 10);
        assertEquals(110, newStock.getQuantity());
    }

    @Test
    @Transactional
    void it_should_be_possible_to_order_from_the_stock() throws InvalidQuantityException {
        Stock newStock = this.sut.order(this.stock.getId(), 10);
        assertEquals(90, newStock.getQuantity());
    }

}
