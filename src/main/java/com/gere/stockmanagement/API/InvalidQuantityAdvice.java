package com.gere.stockmanagement.API;

import com.gere.stockmanagement.Domain.InvalidQuantityException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class InvalidQuantityAdvice {

    /**
     * @param ex an exception of type {@link InvalidQuantityException}
     * @return the exception message
     */
    @ResponseBody
    @ExceptionHandler(InvalidQuantityException.class)
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    String invalidQuantityHandler(InvalidQuantityException ex) {
        return ex.getMessage();
    }

}
