package com.gere.stockmanagement.API;

import com.gere.stockmanagement.Domain.Order;
import com.gere.stockmanagement.Services.OrderService;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {

    private final OrderService orderService;
    private final OrderResourceAssembler assembler;

    /**
     * @param orderService the service to manage {@link Order}s
     * @param assembler {@link OrderResourceAssembler} to create hateoas compliant json responses
     */
    public OrderController(OrderService orderService, OrderResourceAssembler assembler) {
        this.orderService = orderService;
        this.assembler = assembler;
    }


    /**
     * @param order the new {@link Order} to execute
     * @return json representation of an {@link Order}
     */
    @PostMapping("/orders")
    EntityModel<Order> newOrder(@RequestBody Order order) {
        return this.assembler.toModel(this.orderService.execute(order));
    }

}
