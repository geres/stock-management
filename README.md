# Stock management application

The application is based on Spring Boot. It uses an in memory H2 database. So it is self contained.

Build tool is Gradle.

Some dummy database data is preloaded in the LoadDatabase class.

Tests are provided in `/src/test/` folder.

By default the application run on port **8090**, the value can be changed in file `/src/main/resources/application.properties`  

## Available REST routes
`GET /products` list all available products with stock information

`GET /products/{id}` list a single product with stock information

`PUT /products/{id}/refill` update the quantity of the stock for the given product. Json request format is:

        {
	        "quantity": 10
        }

`POST /orders` post a new order. Json request format is: 

        {
	        "productId": 100,
	        "quantity": 12
        }



