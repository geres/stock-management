package com.gere.stockmanagement.API;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gere.stockmanagement.Domain.Order;
import com.gere.stockmanagement.Domain.Product;
import com.gere.stockmanagement.Domain.Stock;
import com.gere.stockmanagement.Repositories.ProductRepository;
import com.gere.stockmanagement.Repositories.StockRepository;
import com.gere.stockmanagement.StockManagementApplication;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = StockManagementApplication.class)
@AutoConfigureMockMvc
class OrderControllerTest {
    @Autowired
    private MockMvc mvc;
    private Stock stock;
    private Product product;
    private @Autowired
    StockRepository stockRepository;
    private @Autowired
    ProductRepository productRepository;

    @BeforeEach
    void setUp() {
        this.stock = new Stock();
        this.product = new Product("name", "description");
        this.product.addStock(this.stock);
        this.stockRepository.save(stock);
        this.productRepository.save(product);
    }

    @Test
    void it_should_create_an_order() throws Exception {
        Order order = new Order(this.product.getId(), 10);
        mvc.perform(post("/orders")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(order)))
                .andExpect(status().isOk());
    }

    @Test
    void it_should_fail_with_invalid_quantity() throws Exception {
        Order order = new Order(this.product.getId(), 101);
        mvc.perform(post("/orders")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(order)))
                .andExpect(status().is4xxClientError());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}