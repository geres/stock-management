package com.gere.stockmanagement.API;

import com.gere.stockmanagement.Domain.Product;
import com.gere.stockmanagement.Domain.Stock;
import com.gere.stockmanagement.Repositories.ProductRepository;
import com.gere.stockmanagement.Repositories.StockRepository;
import com.gere.stockmanagement.Services.OrderService;
import com.gere.stockmanagement.Services.StockService;
import com.gere.stockmanagement.StockManagementApplication;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = StockManagementApplication.class)
@AutoConfigureMockMvc
public class ProductControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private StockRepository stockRepository;
    @MockBean
    private ProductRepository productRepository;
    @MockBean
    private StockService stockService;
    @MockBean
    private OrderService orderService;

    @SpyBean
    private StockResourceAssembler stockResourceAssembler;

    @SpyBean
    private OrderResourceAssembler orderResourceAssembler;


    @BeforeEach
    void setUp() {

        Product a = spy(new Product("a product", "a description"));
        doReturn(1L).when(a).getId();
        Product b = spy(new Product("b product", "b description"));
        doReturn(2L).when(a).getId();
        Stock aStock = spy(new Stock());
        doReturn(1L).when(aStock).getId();
        Stock bStock = spy(new Stock());
        doReturn(2L).when(bStock).getId();
        a.addStock(aStock);
        b.addStock(bStock);
        given(this.stockRepository.findAll()).willReturn(
                Arrays.asList(aStock, bStock));
        given(this.productRepository.findAll()).willReturn(
                Arrays.asList(a, b));
        given(this.productRepository.findById(1L)).willReturn(java.util.Optional.of(a));
        given(this.stockRepository.findById(1L)).willReturn(java.util.Optional.of(aStock));
    }

    @Test
    void it_should_return_the_list_of_product() throws Exception {
        mvc.perform(get("/products"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.products[0].name", is("a product")))
                .andExpect(jsonPath("$._embedded.products[0].id", is(1)))
                .andExpect(jsonPath("$._embedded.products[0]._links.self.href", is("http://localhost/products/1")))
                .andExpect(jsonPath("$._embedded.products[1].name", is("b product")))
                .andExpect(jsonPath("$._embedded.products[1].id", is(2)))
                .andExpect(jsonPath("$._embedded.products[1]._links.self.href", is("http://localhost/products/2")));
    }

    @Test
    void it_should_return_a_single_product() throws Exception {
        mvc.perform(get("/products/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("a product")))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$._links.products.href", is("http://localhost/products")));
    }
}
