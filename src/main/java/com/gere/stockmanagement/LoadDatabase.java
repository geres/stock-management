package com.gere.stockmanagement;

import com.gere.stockmanagement.Domain.Product;
import com.gere.stockmanagement.Domain.Stock;
import com.gere.stockmanagement.Repositories.ProductRepository;
import com.gere.stockmanagement.Repositories.StockRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
@Slf4j
public class LoadDatabase {
    /**
     * Some initial data to fill the in memory database
     *
     * @param productRepository
     * @param stockRepository
     * @return
     */
    @Bean
    CommandLineRunner initDatabase(ProductRepository productRepository, StockRepository stockRepository) {
        return args -> {
            Product a = new Product("a", "a product");
            Product b = new Product("b", "b product");
            Stock aStock = new Stock();
            Stock bStock = new Stock();
            Stock cStock = new Stock();

            log.info("Before " + aStock.getId());
            log.info("Before " + bStock.getId());
            log.info("Before " + cStock.getId());

            a.addStock(cStock);
            b.addStock(bStock);


            log.info("Creating " + stockRepository.save(bStock));
            log.info("Creating " + stockRepository.save(cStock));

            log.info("Creating " + productRepository.save(a));
            log.info("Creating " + productRepository.save(b));
        };
    }
}
