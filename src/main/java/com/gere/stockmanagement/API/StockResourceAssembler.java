package com.gere.stockmanagement.API;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class StockResourceAssembler implements RepresentationModelAssembler<StockWrapper, EntityModel<StockWrapper>> {

    /**
     * Return an hateoas representation of single {@link EntityModel} of {@link StockWrapper}
     *
     * @param stock the REST representation of stock and product
     * @return the rest representation
     */
    @Override
    public EntityModel<StockWrapper> toModel(StockWrapper stock) {

        return new EntityModel<>(stock,
                linkTo(methodOn(ProductController.class).findOne(stock.getId())).withSelfRel(),
                linkTo(methodOn(ProductController.class).findAll()).withRel("products"));
    }

    /**
     * Return a collection of {@link StockWrapper} for the aggregate route
     *
     * @param products a list of products
     * @return the collection
     */
    @Override
    public CollectionModel<EntityModel<StockWrapper>> toCollectionModel(Iterable<? extends StockWrapper> products) {

        List<EntityModel<StockWrapper>> productList = StreamSupport.stream(products.spliterator(), false)
                .map(this::toModel)
                .collect(Collectors.toList());
        return new CollectionModel<>(productList,
                linkTo(methodOn(ProductController.class).findAll()).withSelfRel());
    }
}
