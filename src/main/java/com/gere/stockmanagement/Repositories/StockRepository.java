package com.gere.stockmanagement.Repositories;

import com.gere.stockmanagement.Domain.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StockRepository extends JpaRepository<Stock, Long> {
}
