package com.gere.stockmanagement.API;

import com.gere.stockmanagement.Domain.InvalidQuantityException;
import com.gere.stockmanagement.Domain.Product;
import com.gere.stockmanagement.Domain.ProductNotFoundException;
import com.gere.stockmanagement.Domain.Stock;
import com.gere.stockmanagement.Repositories.StockRepository;
import com.gere.stockmanagement.Services.StockService;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ProductController {

    private final StockRepository stockRepository;

    private final StockResourceAssembler assembler;
    private final StockService stockService;

    /**
     * @param stockRepository repository for {@link Stock}s
     * @param stockService    service for {@link Stock} operations
     * @param assembler       hateos representation generator
     */
    ProductController(StockRepository stockRepository, StockService stockService, StockResourceAssembler assembler) {
        this.stockRepository = stockRepository;
        this.stockService = stockService;
        this.assembler = assembler;
    }

    /**
     * The main route to list all the available products with stock
     *
     * @return Json hateoas representation of products and stock
     */
    @GetMapping("/products")
    CollectionModel<EntityModel<StockWrapper>> findAll() {
        List<Stock> stockList = stockRepository.findAll();
        List<StockWrapper> stockWrappers = stockList
                .stream()
                .map(StockWrapper::new)
                .collect(Collectors.toList());
        return assembler.toCollectionModel(stockWrappers);
    }

    /**
     * The route for a single product
     *
     * @param id the id of {@link Stock} or {@link Product} (they share unique identifier since it is a one-to-one relationship)
     * @return hateoas representation
     */
    @GetMapping("/products/{id}")
    EntityModel<StockWrapper> findOne(@PathVariable long id) {
        Stock stock = stockRepository.findById(id)
                .orElseThrow(() -> new ProductNotFoundException(id));
        StockWrapper wrapper = new StockWrapper(stock);
        return assembler.toModel(wrapper);
    }

    /**
     * @param id the identifier of a {@link Stock}
     * @param refill Request object with refill quantity
     * @return same as findOne method
     */
    @PutMapping("/products/{id}/refill")
    EntityModel<StockWrapper> refill(@PathVariable long id, @RequestBody RefillRequest refill) throws InvalidQuantityException {
        StockWrapper wrapper = new StockWrapper(this.stockService.refill(id, refill.getQuantity()));
        return assembler.toModel(wrapper);
    }
}
