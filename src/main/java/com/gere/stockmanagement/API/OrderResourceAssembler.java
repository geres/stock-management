package com.gere.stockmanagement.API;

import com.gere.stockmanagement.Domain.Order;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
public class OrderResourceAssembler implements RepresentationModelAssembler<Order, EntityModel<Order>> {
    /**
     * Return an hateoas representation of an {@link Order}
     *
     * @param order an {@link Order}
     * @return the REST representation of an order
     */
    @Override
    public EntityModel<Order> toModel(Order order) {
        return new EntityModel<>(order);
    }
}
