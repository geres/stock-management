package com.gere.stockmanagement.Services;

import com.gere.stockmanagement.Domain.InvalidQuantityException;
import com.gere.stockmanagement.Domain.Order;
import com.gere.stockmanagement.Domain.Stock;
import com.gere.stockmanagement.Repositories.OrderRepository;
import org.springframework.stereotype.Service;

/**
 * All the actions that involve an {@link Order}
 */
@Service
public class OrderService {

    private final StockService stockService;
    private final OrderRepository orderRepository;

    /**
     * Service to orchestrated order related operations
     *
     * @param orderRepository
     * @param stockService
     */
    public OrderService(OrderRepository orderRepository, StockService stockService) {
        this.stockService = stockService;
        this.orderRepository = orderRepository;
    }

    /**
     * Execute and persist an order
     * @param order
     * @return the newly persisted order
     * @throws InvalidQuantityException
     */
    public Order execute(Order order) throws InvalidQuantityException {
        Stock stock = stockService.order(order.getProductId(), order.getQuantity());
        return this.orderRepository.save(order);
    }
}
