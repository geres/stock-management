package com.gere.stockmanagement.Domain;

/**
 * To be thrown when there is a request for a Stock / Product that doesn't exist
 */
public class ProductNotFoundException extends RuntimeException {
    /**
     * @param id the
     */
    public ProductNotFoundException(long id) {
        super(String.format("Could not find a product with id: %d", id));
    }
}
