package com.gere.stockmanagement.API;

/**
 * Represent a request to refill a product {@link com.gere.stockmanagement.Domain.Stock}
 */
public class RefillRequest {

    private int quantity;

    /**
     * @return the quantity to refill
     */
    public int getQuantity() {
        return this.quantity;
    }

    /**
     * @param quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
