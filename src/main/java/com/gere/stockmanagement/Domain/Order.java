package com.gere.stockmanagement.Domain;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "ORDERS")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long productId;
    private int quantity;

    @CreationTimestamp
    private Date createdAt;

    /**
     * zero-parameter constructor needed by Spring wiring
     */
    protected Order() {
    }

    /**
     * @param productId the id of the ordered product
     * @param quantity  how many products are orderd
     */
    public Order(long productId, int quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }
}
