package com.gere.stockmanagement.Repositories;

import com.gere.stockmanagement.Domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
