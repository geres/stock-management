package com.gere.stockmanagement.API;

import com.gere.stockmanagement.Domain.Product;
import com.gere.stockmanagement.Domain.Stock;
import lombok.Data;
import org.springframework.hateoas.server.core.Relation;

/**
 * @ A simple representation of a {@link Product} and a {@link Stock} that is used in the rest API
 * The relation association is used to configure the name of the relationship in the JSON representation.
 */
@Data
@Relation(collectionRelation="products")
public class StockWrapper {
    private long id;
    private String name;
    private String description;
    private int quantity;

    /**
     * @param stock the {@link Stock} that need to be adapter
     */
    public StockWrapper(Stock stock) {
        this.id = stock.getId();
        this.name = stock.getProduct().getName();
        this.description = stock.getProduct().getDescription();
        this.quantity = stock.getQuantity();
    }
}
