package com.gere.stockmanagement.Domain;

/**
 * To be thrown when a stock quantity is invalid (< 0, < available quantity)
 */
public class InvalidQuantityException extends RuntimeException {
    /**
     * @param quantity the invalid quantity
     */
    public InvalidQuantityException(int quantity) {
        super(String.format("The quantity %d is not valid.", quantity));
    }
}
