package com.gere.stockmanagement.Domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class StockTest {
    private Stock stock;
    @BeforeEach
    void setUp() {
        this.stock = new Stock();
    }

    @Test
    void it_should_update_stock_quantity_when_refilled() throws InvalidQuantityException {
        this.stock.refill(10);
        assertEquals(110, this.stock.getQuantity());
    }

    @Test
    void it_should_not_accept_negative_quantity_when_refilled() {
        assertThrows(InvalidQuantityException.class, () -> this.stock.refill(-10));
    }

    @Test
    void it_should_update_stock_quantity_when_stock_is_bought() throws InvalidQuantityException {
        this.stock.remove(10);
        assertEquals(90, this.stock.getQuantity());
    }

    @Test
    void it_should_not_accept_negative_quantity_when_stock_is_bought() {
        assertThrows(InvalidQuantityException.class, () -> this.stock.remove(-11));
    }

    @Test
    void it_should_not_be_possible_to_buy_more_stock_then_available() {
        assertThrows(InvalidQuantityException.class, () -> this.stock.remove(101));
        assertEquals(100, this.stock.getQuantity());
    }
}