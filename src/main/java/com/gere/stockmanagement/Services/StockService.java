package com.gere.stockmanagement.Services;

import com.gere.stockmanagement.Domain.ProductNotFoundException;
import com.gere.stockmanagement.Domain.InvalidQuantityException;
import com.gere.stockmanagement.Domain.Stock;
import com.gere.stockmanagement.Repositories.StockRepository;
import org.springframework.stereotype.Service;

/**
 * All the actions that can be performed on a {@link Stock}
 */
@Service
public class StockService {
    private final StockRepository stockRepository;

    /**
     * @param stockRepository the repository of {@link Stock}
     */
    public StockService(StockRepository stockRepository) {
        this.stockRepository = stockRepository;
    }

    /**
     * @param stockId  the stock identifier
     * @param quantity the quantity to refill
     * @return the updated stock
     */
    public Stock refill(long stockId, int quantity) throws InvalidQuantityException {
        Stock stock = this.getStock(stockId);
        stock.refill(quantity);
        return this.stockRepository.save(stock);
    }

    /**
     * @param stockId  the stock identifier
     * @param quantity the quantity to order
     * @return the updated stock
     */
    public Stock order(long stockId, int quantity) throws InvalidQuantityException {
        Stock stock = this.getStock(stockId);
        stock.remove(quantity);
        return this.stockRepository.save(stock);
    }

    /**
     * @param stockId the id of the {@link Stock}
     * @return the founded Stock
     */
    private Stock getStock(long stockId) {
        return stockRepository.findById(stockId)
                .orElseThrow(() -> new ProductNotFoundException(stockId));
    }

}
